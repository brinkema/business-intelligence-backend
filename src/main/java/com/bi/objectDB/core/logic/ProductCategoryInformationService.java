package com.bi.objectDB.core.logic;

import com.bi.objectDB.core.dto.CategoryInformation;
import com.bi.objectDB.core.dto.SupplierProductsByCategory;
import com.bi.objectDB.core.entities.ProductCategory;
import com.bi.objectDB.core.inbound.ProductCategoryInformationProvider;
import com.bi.objectDB.core.outbound.ProductCategoryRepository;

import java.util.List;

public class ProductCategoryInformationService implements ProductCategoryInformationProvider {

    private ProductCategoryRepository productCategoryRepository;

    public ProductCategoryInformationService(ProductCategoryRepository productCategoryRepository) {
        this.productCategoryRepository = productCategoryRepository;
    }

    @Override
    public List<ProductCategory> findAllCategroies() {
        return productCategoryRepository.findAll();
    }

    @Override
    public List<CategoryInformation> countProductsByCategory() {
        return productCategoryRepository.countProductsByCategory();
    }

    @Override
    public List<SupplierProductsByCategory> countProductCategoriesBySupplierId(Integer supplierId) {
        return productCategoryRepository.countProductCategoriesBySupplierId(supplierId);
    }

    @Override
    public Long getCategoryCount() {
        return productCategoryRepository.count();
    }
}

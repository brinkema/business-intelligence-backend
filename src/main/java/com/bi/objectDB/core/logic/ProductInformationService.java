package com.bi.objectDB.core.logic;

import com.bi.objectDB.core.dto.SupplierCategoryCount;
import com.bi.objectDB.core.dto.SupplierProductsByCategory;
import com.bi.objectDB.core.entities.Product;
import com.bi.objectDB.core.inbound.ProductInformationProvider;
import com.bi.objectDB.core.outbound.ProductRepository;

import java.util.List;

public class ProductInformationService implements ProductInformationProvider {

    private ProductRepository productRepository;

    public ProductInformationService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product findProductById(Integer productId) {
        Product product = productRepository.findById(productId).orElseThrow(() -> new RuntimeException("Product with id " + productId + "not found"));
        return product;
    }

    @Override
    public List<Product> findBySupplierAndByCategory(Integer supplierId, Integer categoryId) {
        return productRepository.findBySupplierIdAndCategoryId(supplierId, categoryId);
    }

    @Override
    public List<SupplierCategoryCount> findSupplierCategoryCount() {
        return productRepository.findSupplierCategoryCount();
    }

    @Override
    public SupplierCategoryCount findCategoryCountBySupplierId(Integer supplierId) {
        return productRepository.findCategoryCountBySupplierId(supplierId);
    }

    @Override
    public Long getProductCount() {
        return productRepository.count();
    }
}

package com.bi.objectDB.core.logic;

import com.bi.objectDB.core.entities.Customer;
import com.bi.objectDB.core.inbound.CustomerInformationProvider;
import com.bi.objectDB.core.outbound.CustomerRepository;

import java.util.List;

public class CustomerInformationService implements CustomerInformationProvider {

    private CustomerRepository customerRepository;

    public CustomerInformationService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> findAllCustomer() {
        return customerRepository.findAll();
    }

    @Override
    public Customer findCustomerById(Integer customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new RuntimeException("Customer with id " + customerId + " not found"));
        return customer;
    }

    @Override
    public Long getCustomerCount() {
        return customerRepository.count();
    }
}

package com.bi.objectDB.core.logic;

import com.bi.objectDB.core.entities.CustomerOrder;
import com.bi.objectDB.core.inbound.CustomerOrderInformationProvider;
import com.bi.objectDB.core.outbound.CustomerOrderRepository;

import java.util.List;

public class CustomerOrderInformationService implements CustomerOrderInformationProvider {

    private CustomerOrderRepository customerOrderRepository;

    public CustomerOrderInformationService(CustomerOrderRepository customerOrderRepository) {
        this.customerOrderRepository = customerOrderRepository;
    }

    @Override
    public List<CustomerOrder> findAllOrders() {
        return customerOrderRepository.findAll();
    }

    @Override
    public Long getOrderCount() {
        return customerOrderRepository.count();
    }
}

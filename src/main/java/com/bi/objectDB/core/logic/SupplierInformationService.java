package com.bi.objectDB.core.logic;

import com.bi.objectDB.core.dto.SupplierProductCount;
import com.bi.objectDB.core.dto.SupplierTotalCategories;
import com.bi.objectDB.core.entities.Supplier;
import com.bi.objectDB.core.inbound.SupplierInformationProvider;
import com.bi.objectDB.core.outbound.SupplierRepository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

public class SupplierInformationService implements SupplierInformationProvider {

    private SupplierRepository supplierRepository;

    public SupplierInformationService(SupplierRepository supplierRepository) {
        this.supplierRepository = supplierRepository;
    }

    @Override
    public List<Supplier> findAllSupplier() {
        return supplierRepository.findAll();
    }

    @Override
    public Supplier findBySupplierId(Integer supplierId) {
        Supplier supplier = supplierRepository.findById(supplierId).orElseThrow(() -> new RuntimeException("Supplier with id " + supplierId + "not found"));
        return supplier;
    }

    @Override
    public List<SupplierProductCount> findProductCount() {
        return supplierRepository.findProductCount();
    }

    @Override
    public List<SupplierTotalCategories> findTotalCategories() {
        return supplierRepository.findTotalCategories();
    }

    @RequestMapping("count")
    public Long getSupplierCount() {
        return supplierRepository.count();
    }
}

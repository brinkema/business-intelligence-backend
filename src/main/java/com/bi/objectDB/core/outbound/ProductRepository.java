package com.bi.objectDB.core.outbound;

import com.bi.objectDB.core.dto.SupplierCategoryCount;
import com.bi.objectDB.core.dto.SupplierProductsByCategory;
import com.bi.objectDB.core.dto.SupplierTotalCategories;
import com.bi.objectDB.core.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query(value = "select p from Product p where p.supplier.supplierId = :supplierId and p.category.categoryId = :categoryId")
    List<Product> findBySupplierIdAndCategoryId(@Param("supplierId") Integer supplierId, @Param("categoryId") Integer categoryId);

    @Query(value = "select " +
                    "new com.bi.objectDB.core.dto.SupplierCategoryCount(p.supplier.supplierId, " +
                    "p.supplier.supplierName, " +
                    "sum( p.isCategory(\"Wohnen\") ), " +
                    "sum( p.isCategory(\"Werkzeug\") ), " +
                    "sum( p.isCategory(\"Kleidung\") ), " +
                    "sum( p.isCategory(\"Elektronik\") ), " +
                    "sum( p.isCategory(\"Bürobedarf\") )) " +
                    "from Product p " +
                    "group by p.supplier.supplierId, p.supplier.supplierName")
    List<SupplierCategoryCount> findSupplierCategoryCount();

    @Query(value = "select " +
            "new com.bi.objectDB.core.dto.SupplierCategoryCount(p.supplier.supplierId, " +
            "p.supplier.supplierName, " +
            "sum( p.isCategory(\"Wohnen\") ), " +
            "sum( p.isCategory(\"Werkzeug\") ), " +
            "sum( p.isCategory(\"Kleidung\") ), " +
            "sum( p.isCategory(\"Elektronik\") ), " +
            "sum( p.isCategory(\"Bürobedarf\") )) " +
            "from Product p " +
            "where p.supplier.supplierId = :supplierId " +
            "group by p.supplier.supplierId, p.supplier.supplierName")
    SupplierCategoryCount findCategoryCountBySupplierId(@Param("supplierId") Integer supplierId);
}

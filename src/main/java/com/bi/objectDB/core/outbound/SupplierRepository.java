package com.bi.objectDB.core.outbound;

import com.bi.objectDB.core.dto.SupplierProductCount;
import com.bi.objectDB.core.dto.SupplierProductsByCategory;
import com.bi.objectDB.core.dto.SupplierTotalCategories;
import com.bi.objectDB.core.entities.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SupplierRepository extends JpaRepository<Supplier, Integer> {

    @Query(value = "select " +
            "new com.bi.objectDB.core.dto.SupplierProductCount(s.supplierId, s.supplierName, count(s.products)) " +
            "from Supplier s " +
            "group by s.supplierId, s.supplierName")
    List<SupplierProductCount> findProductCount();

    @Query(value = "select " +
            "new com.bi.objectDB.core.dto.SupplierTotalCategories(s.supplierId, " +
            "s.supplierName, " +
            "count(distinct s.products.category)) " +
            "from Supplier s " +
            "group by s.supplierId, s.supplierName")
    List<SupplierTotalCategories> findTotalCategories();
}

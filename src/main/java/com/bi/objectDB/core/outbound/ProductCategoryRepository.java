package com.bi.objectDB.core.outbound;

import com.bi.objectDB.core.dto.CategoryInformation;
import com.bi.objectDB.core.dto.SupplierProductsByCategory;
import com.bi.objectDB.core.entities.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {

    @Query(value = "select " +
            "new com.bi.objectDB.core.dto.CategoryInformation(p.categoryId, p.categoryName, count(p.products)) " +
            "from ProductCategory p " +
            "group by p.categoryId, p.categoryName")
    List<CategoryInformation> countProductsByCategory();

    @Query(value = "select " +
            "new com.bi.objectDB.core.dto.SupplierProductsByCategory(c.categoryId, " +
            "c.categoryName, " +
            "count(c.categoryName)) " +
            "from ProductCategory c " +
            "where c.products.supplier.supplierId = :supplierId " +
            "group by c.categoryId, c.categoryName")
    List<SupplierProductsByCategory> countProductCategoriesBySupplierId(@Param("supplierId") Integer supplierId);
}

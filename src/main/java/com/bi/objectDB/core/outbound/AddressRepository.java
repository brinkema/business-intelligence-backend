package com.bi.objectDB.core.outbound;

import com.bi.objectDB.core.entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}

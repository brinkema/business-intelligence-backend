package com.bi.objectDB.core.dto;

public class SupplierProductCount {

    private Integer supplierId;
    private String supplierName;
    private Long product_count;

    public SupplierProductCount(Integer supplierId, String supplierName, Long product_count) {
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.product_count = product_count;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public Long getProduct_count() {
        return product_count;
    }
}

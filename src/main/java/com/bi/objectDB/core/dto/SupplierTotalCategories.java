package com.bi.objectDB.core.dto;

public class SupplierTotalCategories {

    private Integer supplierId;
    private String supplierName;
    private Long categoryCount;

    public SupplierTotalCategories(Integer supplierId, String supplierName, Long categoryCount) {
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.categoryCount = categoryCount;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public Long getCategoryCount() {
        return categoryCount;
    }
}

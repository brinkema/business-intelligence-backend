package com.bi.objectDB.core.dto;

public class SupplierCategoryCount {

    private Integer supplierId;
    private String supplierName;
    private Long wohnen;
    private Long werkzeug;
    private Long kleidung;
    private Long elektronik;
    private Long bürobedarf;

    public SupplierCategoryCount(Integer supplierId, String supplierName, Long wohnen, Long werkzeug, Long kleidung, Long elektronik, Long bürobedarf) {
        this.supplierId = supplierId;
        this.supplierName = supplierName;
        this.wohnen = wohnen;
        this.werkzeug = werkzeug;
        this.kleidung = kleidung;
        this.elektronik = elektronik;
        this.bürobedarf = bürobedarf;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public Long getWohnen() {
        return wohnen;
    }

    public Long getWerkzeug() {
        return werkzeug;
    }

    public Long getKleidung() {
        return kleidung;
    }

    public Long getElektronik() {
        return elektronik;
    }

    public Long getBürobedarf() {
        return bürobedarf;
    }
}

package com.bi.objectDB.core.dto;

public class CategoryInformation {

    private Integer categoryId;
    private String categoryName;
    private Long productCount;

    public CategoryInformation(Integer categoryId, String categoryName, Long productCount) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.productCount = productCount;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Long getProductCount() {
        return productCount;
    }
}

package com.bi.objectDB.core.inbound;

import com.bi.objectDB.core.entities.Customer;

import java.util.List;

public interface CustomerInformationProvider {
    List<Customer> findAllCustomer();
    Customer findCustomerById(Integer customerId);
    Long getCustomerCount();
}

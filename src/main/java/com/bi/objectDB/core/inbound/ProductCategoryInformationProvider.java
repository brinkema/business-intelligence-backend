package com.bi.objectDB.core.inbound;

import com.bi.objectDB.core.dto.CategoryInformation;
import com.bi.objectDB.core.dto.SupplierProductsByCategory;
import com.bi.objectDB.core.entities.ProductCategory;

import java.util.List;

public interface ProductCategoryInformationProvider {
    List<ProductCategory> findAllCategroies();
    List<CategoryInformation> countProductsByCategory();
    List<SupplierProductsByCategory> countProductCategoriesBySupplierId(Integer supplierId);
    Long getCategoryCount();
}

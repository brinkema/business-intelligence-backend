package com.bi.objectDB.core.inbound;

import com.bi.objectDB.core.dto.SupplierProductCount;
import com.bi.objectDB.core.dto.SupplierTotalCategories;
import com.bi.objectDB.core.entities.Supplier;

import java.util.List;

public interface SupplierInformationProvider {
    List<Supplier> findAllSupplier();
    Supplier findBySupplierId(Integer supplierId);
    List<SupplierProductCount> findProductCount();
    List<SupplierTotalCategories> findTotalCategories();
    Long getSupplierCount();
}

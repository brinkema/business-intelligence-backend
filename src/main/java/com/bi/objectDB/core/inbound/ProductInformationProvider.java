package com.bi.objectDB.core.inbound;

import com.bi.objectDB.core.dto.SupplierCategoryCount;
import com.bi.objectDB.core.entities.Product;

import java.util.List;

public interface ProductInformationProvider {
    List<Product> findAllProducts();
    Product findProductById(Integer productId);
    List<Product> findBySupplierAndByCategory(Integer supplierId, Integer categoryId);
    List<SupplierCategoryCount> findSupplierCategoryCount();
    SupplierCategoryCount findCategoryCountBySupplierId(Integer supplierId);
    Long getProductCount();
}

package com.bi.objectDB.core.inbound;

import com.bi.objectDB.core.entities.CustomerOrder;

import java.util.List;

public interface CustomerOrderInformationProvider {
    List<CustomerOrder> findAllOrders();
    Long getOrderCount();
}

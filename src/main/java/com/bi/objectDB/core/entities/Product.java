package com.bi.objectDB.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Product {

    @Id
    @GeneratedValue
    private int productId;

    private String productName;

    private Double price;

    @ManyToOne
    private ProductCategory category;

    @ManyToOne
    @JsonIgnore
    private Supplier supplier;

    public int getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Integer isCategory(String category) {
        if (this.category.getCategoryName().equals(category)) {
            return 1;
        }
        return 0;
    }
}
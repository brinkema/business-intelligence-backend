package com.bi.objectDB.core.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
public class CustomerOrder {

    @Id
    @GeneratedValue
    private int customerOrderId;

    private Date date;

    private Boolean isSent;

    private Boolean isPaid;

    @OneToMany
    @JoinColumn(name = "orders")
    private List<Product> orders;

    public int getCustomerOrderId() {
        return customerOrderId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getSent() {
        return isSent;
    }

    public void setSent(Boolean sent) {
        isSent = sent;
    }

    public Boolean getPaid() {
        return isPaid;
    }

    public void setPaid(Boolean paid) {
        isPaid = paid;
    }

    public List<Product> getOrders() {
        return orders;
    }

    public void addProduct(Product product) {
        if (orders == null) {
            orders = new ArrayList<>();
        }
        orders.add(product);
    }
}

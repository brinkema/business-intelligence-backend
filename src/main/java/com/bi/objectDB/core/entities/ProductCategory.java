package com.bi.objectDB.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
public class ProductCategory {

    @Id
    @GeneratedValue
    private int categoryId;

    private String categoryName;

    @OneToMany(mappedBy = "category")
    @JoinColumn(name = "products")
    @JsonIgnore
    private Set<Product> products;

    public int getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Set<Product> getProducts() {
        return products;
    }
}

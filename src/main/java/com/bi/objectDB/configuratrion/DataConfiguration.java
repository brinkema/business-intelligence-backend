package com.bi.objectDB.configuratrion;

import com.bi.objectDB.core.entities.*;
import com.bi.objectDB.core.outbound.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

//@Configuration
public class DataConfiguration {

//    @Bean
    public CommandLineRunner runner(CustomerRepository customerRepository,
                                    CustomerOrderRepository customerOrderRepository,
                                    SupplierRepository supplierRepository,
                                    ProductRepository productRepository,
                                    ProductCategoryRepository productCategoryRepository) {

        List<Customer> customerList = new ArrayList<>();
        for (int i = 0; i <= 999; i++) {
            customerList.add(new Customer());
        }

        List<Address> addressList = new ArrayList<>();
        for (int i = 0; i <= 1004; i++) {
            addressList.add(new Address());
        }

        List<Product> products_buero = new ArrayList<>();
        List<Product> products_electronic = new ArrayList<>();
        List<Product> products_living = new ArrayList<>();
        List<Product> products_tools = new ArrayList<>();
        List<Product> products_clothing = new ArrayList<>();

        for (int i = 0; i <= 9; i++) {
            products_buero.add(new Product());
            products_electronic.add(new Product());
            products_clothing.add(new Product());
            products_living.add(new Product());
            products_tools.add(new Product());
        }

        List<ProductCategory> productCategoryList = new ArrayList<>();
        for (int i = 0; i <= 4; i++) {
            productCategoryList.add(new ProductCategory());
        }

        List<Supplier> supplierList = new ArrayList<>();
        for (int i = 0; i <= 4; i++) {
            supplierList.add(new Supplier());
        }

        List<String> firstNames = new ArrayList<>();
        List<String> lastNames = new ArrayList<>();

        List<String> streets = new ArrayList<>();
        List<Integer> numbers = new ArrayList<>();
        List<String> cities = new ArrayList<>();
        List<Integer> zips = new ArrayList<>();
        List<String> phones = new ArrayList<>();

        List<String> categories = new ArrayList<>();
        List<String> buero = new ArrayList<>();
        List<String> electronic = new ArrayList<>();
        List<String> living = new ArrayList<>();
        List<String> tools = new ArrayList<>();
        List<String> clothing = new ArrayList<>();

        List<String> supplier = new ArrayList<>();

        // first names
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\customer_firstNames.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                firstNames.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // last names
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\customer_lastNames.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                lastNames.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // streets
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\street_names.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                streets.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // street numbers
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\street_numbers.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                numbers.add(Integer.valueOf(line));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // cities
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\city_names.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                cities.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // zip codes
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\zip_codes.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                zips.add(Integer.valueOf(line));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // phone numbers
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\phone_numbers.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                phones.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // product categories
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\product_categories.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                categories.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // products buero
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\products_buero.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                buero.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // products living
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\products_wohnen.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                living.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // products electronic
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\products_elektronik.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                electronic.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // products tools
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\products_werkzeug.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                tools.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // products clothing
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\products_kleidung.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                clothing.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // supplier
        try (BufferedReader br = new BufferedReader(new FileReader("D:\\Benutzer\\Max\\Desktop\\Studium\\Master\\Business_Intelligence\\data\\supplier_list.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                supplier.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Random random = new Random();

        return (args) -> {

            System.out.println("STARTING DATA GENERATION...");

            // Customer generation
            for (int i = 0; i <= 999; i++) {
                int randomInteger = random.nextInt(365);
                addressList.get(i).setCity(cities.get(randomInteger));
                addressList.get(i).setNumber(numbers.get(i));
                addressList.get(i).setPhone(phones.get(i));
                addressList.get(i).setZip(zips.get(i));
                addressList.get(i).setStreet(streets.get(i));

                customerList.get(i).setFirstName(firstNames.get(i));
                customerList.get(i).setLastName(lastNames.get(i));
                addressList.get(i).setEmail(customerList.get(i).getFirstName() + "." + customerList.get(i).getLastName() + "@gmail.com");
                customerList.get(i).setAddress(addressList.get(i));
            }

            // Supplier Generation
            for (int i = 0; i < supplier.size(); i++) {
                supplierList.get(i).setSupplierName(supplier.get(i));
                // supplier get the last few addresses not used by customers
                addressList.get(1000+i).setStreet(streets.get(1000+i));
                addressList.get(1000+i).setNumber(numbers.get(1000+i));
                addressList.get(1000+i).setZip(zips.get(1000+i));
                addressList.get(1000+i).setPhone(phones.get(1000+i));
                String mail = supplier.get(i) + "@logistics.com";
                addressList.get(1000+i).setEmail(mail.replace(" ", ""));
                addressList.get(1000+i).setCity(cities.get(365+i));
                supplierList.get(i).setAddress(addressList.get(1000+i));
            }

            // Product Category Generation
            for (int i = 0; i < categories.size(); i++) {
                productCategoryList.get(i).setCategoryName(categories.get(i));
            }

            // Product Generation
            for (int i = 0; i < buero.size(); i++) {
                String[] dataArr = buero.get(i).split(",");
                products_buero.get(i).setProductName(dataArr[0]);
                products_buero.get(i).setPrice(Double.valueOf(dataArr[1]));

                // set category
                for (ProductCategory pc : productCategoryList) {
                    if (pc.getCategoryName().equals("Bürobedarf")) {
                        products_buero.get(i).setCategory(pc);
                    }
                }

                // set supplier
                if (i <= 4) {
                    supplierList.get(0).addProduct(products_buero.get(i));
                    products_buero.get(i).setSupplier(supplierList.get(0));
                }

                if (i > 4 && i <= 7) {
                    supplierList.get(3).addProduct(products_buero.get(i));
                    products_buero.get(i).setSupplier(supplierList.get(3));
                }

                if (i > 7 && i <= buero.size() - 2) {
                    supplierList.get(4).addProduct(products_buero.get(i));
                    products_buero.get(i).setSupplier(supplierList.get(4));
                }

                if (i > buero.size() - 2) {
                    supplierList.get(1).addProduct(products_buero.get(i));
                    products_buero.get(i).setSupplier(supplierList.get(1));
                }
            }

            for (int i = 0; i < tools.size(); i++) {
                String[] dataArr = tools.get(i).split(",");
                products_tools.get(i).setProductName(dataArr[0]);
                products_tools.get(i).setPrice(Double.valueOf(dataArr[1]));

                // set category
                for (ProductCategory pc : productCategoryList) {
                    if (pc.getCategoryName().equals("Werkzeug")) {
                        products_tools.get(i).setCategory(pc);
                    }
                }

                // set supplier
                if (i <= 4) {
                    supplierList.get(0).addProduct(products_tools.get(i));
                    products_tools.get(i).setSupplier(supplierList.get(0));
                }

                if (i > 4 && i <= 7) {
                    supplierList.get(3).addProduct(products_tools.get(i));
                    products_tools.get(i).setSupplier(supplierList.get(3));
                }

                if (i > 7 && i <= tools.size() - 2) {
                    supplierList.get(4).addProduct(products_tools.get(i));
                    products_tools.get(i).setSupplier(supplierList.get(4));
                }

                if (i > tools.size() - 2) {
                    supplierList.get(1).addProduct(products_tools.get(i));
                    products_tools.get(i).setSupplier(supplierList.get(1));
                }
            }

            for (int i = 0; i < clothing.size(); i++) {
                String[] dataArr = clothing.get(i).split(",");
                products_clothing.get(i).setProductName(dataArr[0]);
                products_clothing.get(i).setPrice(Double.valueOf(dataArr[1]));

                // set category
                for (ProductCategory pc : productCategoryList) {
                    if (pc.getCategoryName().equals("Kleidung")) {
                        products_clothing.get(i).setCategory(pc);
                    }
                }

                // set supplier
                if (i <= 4) {
                    supplierList.get(3).addProduct(products_clothing.get(i));
                    products_clothing.get(i).setSupplier(supplierList.get(3));
                }

                if (i > 4 && i <= clothing.size() - 3) {
                    supplierList.get(4).addProduct(products_clothing.get(i));
                    products_clothing.get(i).setSupplier(supplierList.get(4));
                }

                if (i > clothing.size() - 3) {
                    supplierList.get(1).addProduct(products_clothing.get(i));
                    products_clothing.get(i).setSupplier(supplierList.get(1));
                }
            }

            for (int i = 0; i < electronic.size(); i++) {
                String[] dataArr = electronic.get(i).split(",");
                products_electronic.get(i).setProductName(dataArr[0]);
                products_electronic.get(i).setPrice(Double.valueOf(dataArr[1]));

                // set category
                for (ProductCategory pc : productCategoryList) {
                    if (pc.getCategoryName().equals("Elektronik")) {
                        products_electronic.get(i).setCategory(pc);
                    }
                }

                // set supplier
                if (i <= 4) {
                    supplierList.get(2).addProduct(products_electronic.get(i));
                    products_electronic.get(i).setSupplier(supplierList.get(2));
                }

                if (i > 4 && i <= 7) {
                    supplierList.get(3).addProduct(products_electronic.get(i));
                    products_electronic.get(i).setSupplier(supplierList.get(3));
                }

                if (i > 7 && i <= electronic.size() - 2) {
                    supplierList.get(4).addProduct(products_electronic.get(i));
                    products_electronic.get(i).setSupplier(supplierList.get(4));
                }

                if (i > electronic.size() - 2) {
                    supplierList.get(1).addProduct(products_electronic.get(i));
                    products_electronic.get(i).setSupplier(supplierList.get(1));
                }
            }

            for (int i = 0; i < living.size(); i++) {
                String[] dataArr = living.get(i).split(",");
                products_living.get(i).setProductName(dataArr[0]);
                products_living.get(i).setPrice(Double.valueOf(dataArr[1]));

                // set category
                for (ProductCategory pc : productCategoryList) {
                    if (pc.getCategoryName().equals("Wohnen")) {
                        products_living.get(i).setCategory(pc);
                    }
                }

                // set supplier
                if (i <= 4) {
                    supplierList.get(0).addProduct(products_living.get(i));
                    products_living.get(i).setSupplier(supplierList.get(0));
                }

                if (i > 4 && i <= 7) {
                    supplierList.get(3).addProduct(products_living.get(i));
                    products_living.get(i).setSupplier(supplierList.get(3));
                }

                if (i > 7 && i <= living.size() - 2) {
                    supplierList.get(4).addProduct(products_living.get(i));
                    products_living.get(i).setSupplier(supplierList.get(4));
                }

                if (i > living.size() - 2) {
                    supplierList.get(1).addProduct(products_living.get(i));
                    products_living.get(i).setSupplier(supplierList.get(1));
                }
            }

            System.out.println("GENERATING CUSTOMER DATA...");
            for (Customer customer : customerList) {
                customerRepository.save(customer);
            }

            System.out.println("GENERATING SUPPLIER DATA...");
            for (Supplier sp : supplierList) {
                supplierRepository.save(sp);
            }

            System.out.println("GENERATING PRODUCT CATEGORIES...");
            for (ProductCategory pc : productCategoryList) {
                productCategoryRepository.save(pc);
            }

            System.out.println("GENERATING PRODUCTS...");
            for (Product product : products_buero) {
                productRepository.save(product);
            }

            for (Product product : products_clothing) {
                productRepository.save(product);
            }

            for (Product product : products_electronic) {
                productRepository.save(product);
            }

            for (Product product : products_living) {
                productRepository.save(product);
            }

            for (Product product : products_tools) {
                productRepository.save(product);
            }

            System.out.println("GENERATING CUSTOMER ORDERS...");
            List<CustomerOrder> orderList = new ArrayList<>();
            List<Product> allProducts = new ArrayList<>();
            allProducts.addAll(products_electronic);
            allProducts.addAll(products_buero);
            allProducts.addAll(products_clothing);
            allProducts.addAll(products_living);
            allProducts.addAll(products_tools);

            // generate 3000 orders
            for (int i = 0; i <= 3000; i++) {
                CustomerOrder order = new CustomerOrder();

                // random amount of products
                int amount_orders = random.nextInt(6);

                // add products to order
                for (int x = 0; x <= amount_orders; x++) {
                    // random index of product list
                    int ndx = random.nextInt(50);
                    order.addProduct(allProducts.get(ndx));
                }

                // date
                long minDay = LocalDate.of(2015, 1, 1).toEpochDay();
                long maxDay = LocalDate.of(2020, 6, 30).toEpochDay();
                long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
                LocalDate randomDate = LocalDate.ofEpochDay(randomDay);
                order.setDate(java.sql.Date.valueOf(randomDate));

                // is paid
                int isPaid = random.nextInt(2);
                order.setPaid(false);
                if (isPaid == 0 || randomDate.isBefore(LocalDate.of(2020, 5,1))) {
                    order.setPaid(true);
                }

                // is sent
                order.setSent(false);
                if (order.getPaid()) {
                    int isSent = random.nextInt(2);
                    if (isSent == 0) {
                        order.setSent(true);
                    }
                }

                // add order to list
                orderList.add(order);
            }

            for (CustomerOrder co : orderList) {
                customerOrderRepository.save(co);
            }


            for (CustomerOrder co : orderList) {
                int cust = random.nextInt(1000);
                customerList.get(cust).addOrder(co);
                customerRepository.save(customerList.get(cust));
            }

            System.out.println("DONE");

        };
    }
}

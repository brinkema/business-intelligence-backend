package com.bi.objectDB.configuratrion;

import com.bi.objectDB.core.inbound.*;
import com.bi.objectDB.core.logic.*;
import com.bi.objectDB.core.outbound.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalConfiguration {

    @Bean
    public CustomerInformationProvider customerInformationProvider(CustomerRepository customerRepository) {
        return new CustomerInformationService(customerRepository);
    }

    @Bean
    public ProductInformationProvider productInformationProvider(ProductRepository productRepository) {
        return new ProductInformationService(productRepository);
    }

    @Bean
    public SupplierInformationProvider supplierInformationProvider(SupplierRepository supplierRepository) {
        return new SupplierInformationService(supplierRepository);
    }

    @Bean
    public ProductCategoryInformationProvider productCategoryInformationProvider(ProductCategoryRepository productCategoryRepository) {
        return new ProductCategoryInformationService(productCategoryRepository);
    }

    @Bean
    public CustomerOrderInformationProvider customerOrderInformationProvider(CustomerOrderRepository customerOrderRepository) {
        return new CustomerOrderInformationService(customerOrderRepository);
    }
}

package com.bi.objectDB.inbound;

import com.bi.objectDB.core.dto.SupplierCategoryCount;
import com.bi.objectDB.core.entities.Product;
import com.bi.objectDB.core.inbound.ProductInformationProvider;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("products")
public class ProductController {

    private ProductInformationProvider productInformationProvider;

    public ProductController(ProductInformationProvider productInformationProvider) {
        this.productInformationProvider = productInformationProvider;
    }

    @RequestMapping("all")
    public List<Product> findAllProducts() {
        return productInformationProvider.findAllProducts();
    }

    @RequestMapping("/{productId}")
    public Product findByProductId(@PathVariable Integer productId) {
        return productInformationProvider.findProductById(productId);
    }

    @RequestMapping("/{supplierId}/{categoryId}")
    public List<Product> findBySupplierIdAndCategoryId(@PathVariable Integer supplierId, @PathVariable Integer categoryId) {
        return productInformationProvider.findBySupplierAndByCategory(supplierId, categoryId);
    }

    @RequestMapping("/category-count")
    public List<SupplierCategoryCount> findSupplierCategoryCount() {
        return productInformationProvider.findSupplierCategoryCount();
    }

    @RequestMapping("/{supplierId}/category-count")
    public SupplierCategoryCount findCategoryCountBySupplierId(@PathVariable Integer supplierId) {
        return productInformationProvider.findCategoryCountBySupplierId(supplierId);
    }

    @RequestMapping("product-count")
    public Long getProductCount() {
        return productInformationProvider.getProductCount();
    }
}

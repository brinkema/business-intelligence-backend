package com.bi.objectDB.inbound;

import com.bi.objectDB.core.entities.Customer;
import com.bi.objectDB.core.entities.CustomerOrder;
import com.bi.objectDB.core.inbound.CustomerOrderInformationProvider;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("orders")
public class CustomerOrderController {

    private CustomerOrderInformationProvider customerOrderInformationProvider;

    public CustomerOrderController(CustomerOrderInformationProvider customerOrderInformationProvider) {
        this.customerOrderInformationProvider = customerOrderInformationProvider;
    }

    @RequestMapping("all")
    public List<CustomerOrder> findAllOrders() {
        return customerOrderInformationProvider.findAllOrders();
    }

    @RequestMapping("order-count")
    public Long countOrders() {
        return customerOrderInformationProvider.getOrderCount();
    }
}

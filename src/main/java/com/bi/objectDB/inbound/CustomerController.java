package com.bi.objectDB.inbound;

import com.bi.objectDB.core.entities.Customer;
import com.bi.objectDB.core.inbound.CustomerInformationProvider;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("customers")
public class CustomerController {

    private CustomerInformationProvider customerInformationProvider;

    public CustomerController(CustomerInformationProvider customerInformationProvider) {
        this.customerInformationProvider = customerInformationProvider;
    }

    @RequestMapping("all")
    public List<Customer> findAllCustomer() {
        return customerInformationProvider.findAllCustomer();
    }

    @RequestMapping("/{customerId}")
    public Customer findCustomerById(@PathVariable Integer customerId) {
        return customerInformationProvider.findCustomerById(customerId);
    }

    @RequestMapping("customer-count")
    public Long countCustomers() {
        return customerInformationProvider.getCustomerCount();
    }
}

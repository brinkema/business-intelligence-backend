package com.bi.objectDB.inbound;

import com.bi.objectDB.core.dto.CategoryInformation;
import com.bi.objectDB.core.dto.SupplierProductsByCategory;
import com.bi.objectDB.core.entities.ProductCategory;
import com.bi.objectDB.core.inbound.ProductCategoryInformationProvider;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("categories")
public class ProductCategoryController {

    private ProductCategoryInformationProvider productCategoryInformationProvider;

    public ProductCategoryController(ProductCategoryInformationProvider productCategoryInformationProvider) {
        this.productCategoryInformationProvider = productCategoryInformationProvider;
    }

    @RequestMapping("all")
    public List<ProductCategory> findAllCategories() {
        return productCategoryInformationProvider.findAllCategroies();
    }

    @RequestMapping("count")
    public List<CategoryInformation> countByCategory() {
        return productCategoryInformationProvider.countProductsByCategory();
    }

    @RequestMapping("{supplierId}/category-count")
    public List<SupplierProductsByCategory> countProductCategoriesBySupplierId(@PathVariable Integer supplierId) {
        return productCategoryInformationProvider.countProductCategoriesBySupplierId(supplierId);
    }

    @RequestMapping("category-count")
    public Long getOrderCount() {
        return productCategoryInformationProvider.getCategoryCount();
    }
}

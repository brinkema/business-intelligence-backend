package com.bi.objectDB.inbound;

import com.bi.objectDB.core.dto.SupplierProductCount;
import com.bi.objectDB.core.dto.SupplierTotalCategories;
import com.bi.objectDB.core.entities.Supplier;
import com.bi.objectDB.core.inbound.SupplierInformationProvider;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("suppliers")
public class SupplierController {

    private SupplierInformationProvider supplierInformationProvider;

    public SupplierController(SupplierInformationProvider supplierInformationProvider) {
        this.supplierInformationProvider = supplierInformationProvider;
    }

    @RequestMapping("all")
    public List<Supplier> findAllSupplier() {
        return supplierInformationProvider.findAllSupplier();
    }

    @RequestMapping("/{supplierId}")
    public Supplier findByProductId(@PathVariable Integer supplierId) {
        return supplierInformationProvider.findBySupplierId(supplierId);
    }

    @RequestMapping("/count")
    public List<SupplierProductCount> findProductCount() {
        return supplierInformationProvider.findProductCount();
    }

    @RequestMapping("/total-categories")
    public List<SupplierTotalCategories> findTotalCategories() {
        return supplierInformationProvider.findTotalCategories();
    }

    @RequestMapping("supplier-count")
    public Long getSupplierCount() {
        return supplierInformationProvider.getSupplierCount();
    }
}
